import matplotlib.pyplot as plt
import numpy as np


x1=np.arange(-8,-2,0.0001)
y1=1.5*np.sqrt(x1*x1-4)
x2=np.arange(-8,-2,0.0001)
y2=-1.5*np.sqrt(x2*x2-4)
x3=np.arange(2,8,0.0001)
y3=1.5*np.sqrt(x3*x3-4)
x4=np.arange(2,8,0.0001)
y4=-1.5*np.sqrt(x4*x4-4)


# 设置显示内容
plt.cla()
plt.title("双曲线方程", fontproperties="simsun")

# 设置坐标轴样式
ax = plt.gca()
ax.set_aspect(1)

ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.spines['bottom'].set_position(('data', 0))
ax.yaxis.set_ticks_position('left')
ax.spines['left'].set_position(('data',0))


plt.plot(x1, y1, 'b')
plt.plot(x2,y2, 'b')
plt.plot(x3,y3, 'b')
plt.plot(x4,y4, 'b')
plt.show()