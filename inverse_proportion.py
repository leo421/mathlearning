import matplotlib.pyplot as plt


#创建数组
def frange(_start:float, _end:float, _step:float):
    x = []
    while True:
        x.append(_start)
        _start += _step
        if _start > _end:
            break
    return x

x1 = frange(-10.1, 0, 0.2)
y1 = []
x2 = frange(0.1, 10.1, 0.2)
y2 = []

#根据公式将x值代入得出y值存入y列表中
def f(x,y):
    for i in x:
        a=1/i
        y.append(a)

# 计算生成坐标点集合
f(x1, y1)
f(x2, y2)


# 设置显示内容
plt.title("反比例函数", fontproperties="simsun")

# 设置坐标轴样式
ax = plt.gca()
ax.set_aspect(1)

ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.spines['bottom'].set_position(('data', 0))
ax.yaxis.set_ticks_position('left')
ax.spines['left'].set_position(('data',0))


plt.plot(x1,y1,'b,-')
plt.plot(x2,y2,'b,-')
# plt.savefig("反比例函数.png")
plt.show()
