import matplotlib.pyplot as plt
from math import sin, cos

#创建数组
def frange(_start:float, _end:float, _step:float):
    x = []
    while True:
        x.append(_start)
        _start += _step
        if _start > _end:
            break
    return x

x1 = frange(-10, 10, 0.1)
y1 = []
x2 = frange(-10, 10, 0.1)
y2 = []

#根据公式将x值代入得出y值存入y列表中
def fsine(x,y):
    for i in x:
        a = 2 * sin(i)
        y.append(a)

def fcosine(x,y):
    for i in x:
        a = 2 * cos(i)
        y.append(a)

# 计算生成坐标点集合
fsine(x1, y1)
fcosine(x2, y2)


# 设置显示内容
plt.cla()
plt.title("正余弦函数", fontproperties="simsun")

# 设置坐标轴样式
ax = plt.gca()
ax.set_aspect(1)

ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.spines['bottom'].set_position(('data', 0))
ax.yaxis.set_ticks_position('left')
ax.spines['left'].set_position(('data',0))


# plt.plot(x1, y1, 'g,-')
# plt.plot(x2, y2, 'b,-')
plt.plot(x1, y1, 'g', label='sine')
plt.plot(x2, y2, 'b', label='cosine')
plt.legend(loc=1)
# plt.savefig("正余弦函数.png")
plt.show()
