import matplotlib.pyplot as plt
#创建数组
x = range(-10,16)

y = []

#根据公式将x值代入得出y值存入y列表中
def f(x,y):
    for i in x:
        a = 0.1 * i**2 - 0.5 * i - 5
        y.append(a)

# 计算生成坐标点集合
f(x, y)


# 设置显示内容
plt.title("抛物线函数", fontproperties="simsun")

# 设置坐标轴样式
ax = plt.gca()
ax.set_aspect(1)

ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.spines['bottom'].set_position(('data', 0))
ax.yaxis.set_ticks_position('left')
ax.spines['left'].set_position(('data',0))


plt.plot(x,y,'b,-')
# plt.savefig("抛物线.png")
plt.show()
