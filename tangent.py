import matplotlib.pyplot as plt
from math import pi, tan


#创建数组
def frange(_start:float, _end:float, _step:float):
    x = []
    while True:
        x.append(_start)
        _start += _step
        if _start > _end:
            break
    return x

x1 = frange(-3.14/2 + 0.01 - pi, 3.14/2 - pi - 0.01, 0.01)
x2 = frange(-3.14/2 + 0.01, 3.14/2, 0.01)
x3 = frange(-3.14/2 + 0.01 + pi, 3.14/2 + pi - 0.01, 0.01)

y1 = []
y2 = []
y3 = []

#根据公式将x值代入得出y值存入y列表中
def ftangent(x,y):
    for i in x:
        a = 0.05 * tan(i)
        y.append(a)

# 计算生成坐标点集合
ftangent(x1, y1)
ftangent(x2, y2)
ftangent(x3, y3)


# 设置显示内容
plt.cla()
plt.title("正切函数", fontproperties="simsun")

# 设置坐标轴样式
ax = plt.gca()
ax.set_aspect(1)

ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.spines['bottom'].set_position(('data', 0))
ax.yaxis.set_ticks_position('left')
ax.spines['left'].set_position(('data',0))


# plt.plot(x1, y1, 'g,-')
# plt.plot(x2, y2, 'b,-')
plt.plot(x1, y1, 'g,-')
plt.plot(x2, y2, 'g,-')
plt.plot(x3, y3, 'g,-')
# plt.plot(x2, y2, 'b,-', label='cotangent')
# plt.legend(loc=1)
# plt.savefig("正切函数.png")
plt.show()
